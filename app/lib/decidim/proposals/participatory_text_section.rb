# frozen_string_literal: true

module Decidim
  module Proposals
    # The data store for a Proposal in the Decidim::Proposals component.
    module ParticipatoryTextSection
      extend ActiveSupport::Concern

      LEVELS = {
        section: "section", sub_section: "sub-section", article: "article", table: "table", image: "image", comments_section: "comments-section"
      }.freeze

      included do
        # Public: is this section an :article?
        def article?
          participatory_text_level == LEVELS[:article]
        end

        def section?
          participatory_text_level == LEVELS[:section]
        end

        def sub_section?
          participatory_text_level == LEVELS[:sub_section]
        end

        def table?
          participatory_text_level == LEVELS[:table]
        end

        def image?
          participatory_text_level == LEVELS[:image]
        end

        def comments_section?
          participatory_text_level == LEVELS[:comments_section]
        end
      end
    end
  end
end
