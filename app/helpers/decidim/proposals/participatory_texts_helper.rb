# frozen_string_literal: true

module Decidim
  module Proposals
    # Simple helper to handle markup variations for participatory texts related partials
    module ParticipatoryTextsHelper
      # Returns the title for a given participatory text section.
      #
      # proposal - The current proposal item.
      #
      # Returns a string with the title of the section, subsection or article.
      def preview_participatory_text_section_title(proposal)
        title = decidim_html_escape(present(proposal).title)
        translated = t(proposal.participatory_text_level, scope: "decidim.proposals.admin.participatory_texts.sections", title: title)
        translated.html_safe
      end

      def render_participatory_text_title(participatory_text, proposal_first)
        if proposal_first.nil?
          t("alternative_title", scope: "decidim.proposals.participatory_text_proposal")
        elsif participatory_text
          translated_attribute(participatory_text.title)
        else
          translated_attribute(current_component.name)
        end
      end

      # Public: A formatted collection of mime_type to be used
      # in forms.
      def mime_types_with_document_examples
        links = ""
        accepted_mime_types = Decidim::Proposals::DocToMarkdown::ACCEPTED_MIME_TYPES.keys
        accepted_mime_types.each_with_index do |mime_type, index|
          links += link_to t(".accepted_mime_types.#{mime_type}"),
                           asset_pack_path("media/documents/participatory_text.#{mime_type}"),
                           download: "participatory_text.#{mime_type}"
          links += ", " unless accepted_mime_types.length == index + 1
        end
        links
      end

      def table_from_str(excel_data)
        rows = excel_data.split("\n") # Split by newline for each row
        html = String.new("<table>\n")

        rows.each do |row|
          html << "  <tr>\n"
          cells = row.split("\t") # Split by tab for each cell
          cells.each do |cell|
            html << "    <td>#{cell.strip}</td>\n" # Create table cell with stripped value
          end
          html << "  </tr>\n"
        end

        html << "</table>"
      end

      def generate_string_from_excel(workbook)
        output = String.new # String para armazenar o resultado

        # Supondo que só exista uma planilha, pegar a primeira worksheet
        worksheet = workbook[0]

        # Iterar sobre as linhas da worksheet
        worksheet.each_with_index do |row, _row_index|
          # Para cada linha, iterar sobre as células
          row_values = row.cells.map do |cell|
            cell&.value.to_s.strip # Obter o valor da célula, garantindo que seja uma string e removendo espaços
          end

          # Adicionar a linha formatada com tabulação na string de saída
          output << "#{row_values.join("\t")}\n"
        end

        output.strip # Retorna a string gerada removendo espaços extras no final
      end

      def generate_html_table(workbook)
        html_output = String.new("<table>")

        # Iterate over each worksheet in the workbook
        workbook.worksheets.each do |worksheet|
          # Iterate over each row in the worksheet
          worksheet.each_with_index do |row, _row_index|
            html_output << "<tr>"

            # Iterate over each cell in the row
            row.cells.each do |cell|
              cell_value = cell&.value.to_s # Safely get the cell value as string
              html_output << "<td>#{cell_value}</td>" # Create a table cell
            end

            html_output << "</tr>"
          end
        end

        html_output << "</table><br/>" # Close the table and add space between tables
      end

      def excel_from_record(record)
        Tempfile.open(['downloaded_file', '.xlsx']) do |tempfile|
          tempfile.binmode # Ensure binary mode for Excel files

          # Download the file content from Active Storage and write it to the Tempfile
          tempfile.write(record.download)
          tempfile.rewind # Rewind the Tempfile to the beginning for reading

          RubyXL::Parser.parse(tempfile.path)
        end
      end

      def participatory_text_comments(proposals)
        proposals.find(&:comments_section?)
      end

      def published_proposals
        Decidim::Proposals::Proposal
          .where(component: current_component)
          .published
      end
    end
  end
end
