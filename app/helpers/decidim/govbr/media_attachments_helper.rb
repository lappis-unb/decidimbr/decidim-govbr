# frozen_string_literal: true

module Decidim
  # A Helper to render and link to resources.
  module Govbr
    module MediaAttachmentsHelper
      # Renders the attachments of a Participatory Space that includes the
      # HasAttachments concern.
      #
      # attached_to - The model to render the attachments from.
      #
      # Returns nothing.
      def attachments_for_participatory_space(attached_to)
        render partial: "decidim/govbr/media/attachments", locals: { attached_to: attached_to }
      end

      # Renders the attachment's title.
      # Checks if the attachment's title is translated or not and use
      # the correct render method.
      #
      # attachment - An Attachment object
      #
      # Returns String.
      def attachment_title(attachment)
        attachment.title.is_a?(Hash) ? translated_attribute(attachment.title) : attachment.title
      end

      def blob(signed_id)
        ActiveStorage::Blob.find_signed(signed_id)
      end

      def build_image_attachment(form:, attached_to:)
        return unless form.try(:image_attachment)

        image = form.image_attachment.try(:[], "0") || form.image_attachment.try(:[], :'0')
        file = blob(image["file"] || image[:file])

        Attachment.new(
          title: { 'pt-BR': file.filename },
          attached_to: attached_to,
          file: image["file"], # Define attached_to before this
          content_type: file.content_type,
          file_size: file.byte_size
        )
      end

      def save_image_attachment(form:, attached_to:)
        return unless form.try(:image_attachment)

        @attachment = build_image_attachment(form: form, attached_to: attached_to)

        if has_previous_image_attachment?(attached_to: attached_to)
          attachment_to_delete = attached_to.image_attachment

          Decidim.traceability.perform_action!(:delete, attachment_to_delete, form.current_user) do
            attachment_to_delete.destroy!
          end
        end

        Decidim.traceability.perform_action!(:create, Decidim::Attachment, form.current_user) do
          @attachment.save!
          @attachment
        end
      end

      def has_previous_image_attachment?(attached_to:)
        attached_to.try(:image_attachment).present?
      end

      def new_image_attachment?(form:, attached_to:)
        return true if attached_to.image_attachment.blank? && form.try(:image_attachment).try(:key?, "0")

        form.image_attachment.is_a?(Hash) && form.image_attachment.has_key?("0")
      end
    end
  end
end
