# frozen_string_literal: true

# This validator takes care of ensuring the validated content is
# respectful, doesn't use caps, and overall is meaningful.
class EtiquetteValidator < ActiveModel::EachValidator
  include ActionView::Helpers::SanitizeHelper

  def validate_each(record, attribute, value)
    return if value.blank?

    text_value = strip_tags(value)

    validate_caps_field(record, attribute, text_value)
    validate_marks(record, attribute, text_value)
    validate_caps_first(record, attribute, text_value)
  end

  private

  def validate_caps_field(record, attribute, value)
    record.errors.add(attribute, :too_much_caps) if value.scan(/[A-Z]/).length >= value.length / 4
  end

  def validate_caps_first(record, attribute, value)
    record.errors.add(attribute, :must_start_with_caps) unless value.match?(/\A[A-Z]/)
  end

  def validate_marks(record, attribute, value)
    record.errors.add(attribute, :too_many_marks) unless value.scan(/[!?¡¿]{2,}/).empty?
  end
end
