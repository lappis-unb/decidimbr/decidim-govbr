document.addEventListener("DOMContentLoaded", function() {
  function initializeAddressInput(addressInputId) {
    const addressInput = document.getElementById(addressInputId);
    if (!addressInput) {
      return;
    }

    function ordenarPorNome(estados) {
      return estados.sort((a, b) => {
        if (a.nome < b.nome) return -1;
        if (a.nome > b.nome) return 1;
        return 0;
      });
    }

    function removeAllChildren(parentElement) {
      while (parentElement.firstChild) {
        parentElement.removeChild(parentElement.firstChild);
      }
    }

    function updateAddress(cidades) {
      var city = parseInt(cityInput.value);
      var state = parseInt(stateInput.value);

      for (let i = 0; i < cidades.length; i++) {
        if (cidades[i].id === city) {
          city = cidades[i].nome;
          break;
        }
      }

      for (let i = 0; i < estados.length; i++) {
        if (estados[i].id === state) {
          state = estados[i].nome;
          break;
        }
      }
      preencherCampos(city, state);
    }

    function preencherCampos(cidade, estado) {
      addressInput.value = `${cidade}, ${estado}`;
    }

    const cityInput = document.getElementById('city');
    const stateInput = document.getElementById('state');
    const addressInputDiv = document.getElementById('address_input_div');
    const statesURL = "https://servicodados.ibge.gov.br/api/v1/localidades/estados/";
    const estados = [];
    let cidadesResponde = [];
    let address = addressInput.value ? addressInput.value.split(", ") : [];

    $.ajax({
      url: statesURL,
      type: 'GET',
      success: function(response) {
        let edit = false;
        response = ordenarPorNome(response);

        for (let i = 0; i < response.length; i++) {
          let state = document.createElement("option");
          estados[i] = response[i];
          state.value = response[i].id;
          state.innerHTML = response[i].nome;

          if (address[1] === response[i].nome) {
            state.selected = true;
            edit = true;
          }
          stateInput.appendChild(state);
        }

        if (edit) {
          setCitys();
        }
      },
      error: function(xhr) {}
    });

    function setCitys() {
      const stateId = stateInput.value;
      const cityURL = statesURL.concat(stateId, "/municipios");

      cityInput.disabled = false;
      removeAllChildren(cityInput);
      $.ajax({
        url: cityURL,
        type: 'GET',
        success: function(response) {
          response = ordenarPorNome(response);
          for (let i = 0; i < response.length; i++) {
            let city = document.createElement("option");

            cidadesResponde[i] = response[i];

            city.value = response[i].id;
            city.innerHTML = response[i].nome;
            if (address[0] === response[i].nome) {
              city.selected = true;
            }
            cityInput.appendChild(city);
          }
          updateAddress(cidadesResponde);
        },
        error: function(xhr) {}
      });
    }

    cityInput.addEventListener('change', () => updateAddress(cidadesResponde));
    stateInput.addEventListener('change', setCitys);

  }

  if (document.getElementById('proposal_address')) {
    initializeAddressInput('proposal_address');
  }

  if (document.getElementById('meeting_address')) {
    initializeAddressInput('meeting_address');
  }

  const $checkbox = $("input:checkbox[name$='[has_address]']");

  if ($checkbox.length > 0) {
    const toggleInput = () => {
      if ($checkbox[0].checked) {
        addressInputDiv.style.display = "block";
      } else {
        addressInputDiv.style.display = "none";
      }
    }
    toggleInput();
    $checkbox.on("change", toggleInput);
  }
});