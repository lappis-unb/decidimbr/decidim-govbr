# frozen_string_literal: true

module Decidim
  # A command with all the business logic to create a user through the sign up form.
  class CreateRegistration < Decidim::Command
    # Public: Initializes the command.
    #
    # form - A form object with the params.
    def initialize(form)
      @form = form
    end

    # Executes the command. Broadcasts these events:
    #
    # - :ok when everything is valid.
    # - :invalid if the form wasn't valid and we couldn't proceed.
    #
    # Returns nothing.
    def call
      if form.invalid?
        user = User.has_pending_invitations?(form.current_organization.id, form.email)
        user.invite!(user.invited_by) if user
        return broadcast(:invalid, all_form_errors)
      end

      return broadcast(:invalid, all_form_errors) unless create_user

      send_user_pending_confirmation_notification

      broadcast(:ok, @user)
    rescue ActiveRecord::RecordInvalid
      broadcast(:invalid, all_form_errors)
    end

    private

    attr_reader :form

    def all_form_errors
      result = ''
      result += form.errors.full_messages.join(', ') if form && form.invalid?
      result += @register_form.errors.full_messages.join(', ') if @register_form && @register_form.invalid? # rubocop:disable Rails/HelperInstanceVariable
      result
    end

    # This method is overwritten by decidim-extra_user_fields, check there.
    def create_user
      @user = User.create!(
        email: form.email,
        name: form.name,
        nickname: form.nickname,
        password: form.password,
        password_confirmation: form.password_confirmation,
        organization: form.current_organization,
        tos_agreement: form.tos_agreement,
        newsletter_notifications_at: form.newsletter_at,
        accepted_tos_version: form.current_organization.tos_version,
        locale: form.current_locale
      )
    end

    def send_user_pending_confirmation_notification
      Decidim::Govbr::RegistrationVerificationMailer.confirmation_pending(@user).deliver_later
    end
  end
end
