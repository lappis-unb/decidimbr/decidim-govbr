# frozen_string_literal: true

module Decidim
  module Govbr
    class GrantAuthorizationToGovbrUsers < Decidim::Command
      def call
        grant_authorization_to users_pending_authorization
        broadcast(:ok)
      end

      def grant_authorization_to(users)
        users.each do |user|
          authorization = Decidim::Authorization.find_or_initialize_by(user: user, name: "id_documents")
          authorization&.grant!
        end
      end

      def users_pending_authorization
        @users_pending_authorization ||=
          Decidim::User
          .joins(
            <<~SQL.squish
              LEFT JOIN decidim_authorizations da
                ON da.decidim_user_id = decidim_users.id
                  AND da.id IS NULL
            SQL
          )
          .joins(:identities)
          .limit(1000)
      end
    end
  end
end
