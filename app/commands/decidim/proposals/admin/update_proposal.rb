# frozen_string_literal: true

module Decidim
  module Proposals
    module Admin
      # A command with all the business logic when a user updates a proposal.
      class UpdateProposal < Decidim::Command
        include ::Decidim::AttachmentMethods
        include ::Decidim::MultipleAttachmentsMethods
        include GalleryMethods
        include HashtagsMethods
        include Decidim::Govbr::MediaAttachmentsHelper

        # Public: Initializes the command.
        #
        # form         - A form object with the params.
        # proposal - the proposal to update.
        def initialize(form, proposal)
          @form = form
          @proposal = proposal
          @attached_to = proposal
        end

        # Executes the command. Broadcasts these events:
        #
        # - :ok when everything is valid, together with the proposal.
        # - :invalid if the form wasn't valid and we couldn't proceed.
        #
        # Returns nothing.
        def call
          return broadcast(:invalid) if form.invalid?

          handle_attachment_deletion
          handle_attachment_processing
          handle_gallery_processing

          persist_data

          broadcast(:ok, proposal)
        end

        private

        attr_reader :form, :proposal, :attachment, :gallery

        def handle_attachment_deletion
          delete_attachment(form.attachment) if delete_attachment?
        end

        def handle_attachment_processing
          return unless process_attachments?

          @proposal.attachments.destroy_all

          build_attachment
          return broadcast(:invalid) if attachment_invalid?
        end

        def handle_gallery_processing
          return unless process_gallery?

          build_gallery
          return broadcast(:invalid) if gallery_invalid?
        end

        def persist_data
          transaction do
            update_proposal
            update_proposal_author
            create_gallery if process_gallery?
            create_attachment(weight: first_attachment_weight) if process_attachments?
            photo_cleanup! unless form.try(:image_attachment)
          end
        end

        def update_proposal
          parsed_title = Decidim::ContentProcessor.parse_with_processor(:hashtag, form.title, current_organization: form.current_organization).rewrite
          parsed_body = Decidim::ContentProcessor.parse(form.body, current_organization: form.current_organization).rewrite

          proposal_attributes = {
            title: parsed_title,
            category: form.category,
            scope: form.scope,
            address: form.address,
            latitude: form.latitude,
            longitude: form.longitude,
            is_interactive: form.is_interactive,
            is_hidden: form.is_hidden,
            created_in_meeting: form.created_in_meeting,
            associated_state: form.associated_state
          }

          proposal_attributes[:body] = parsed_body unless proposal.try(:image?)

          Decidim.traceability.update!(
            proposal,
            form.current_user,
            **proposal_attributes
          )

          save_image_attachment(form: form, attached_to: proposal) if new_image_attachment?(form: form, attached_to: proposal)
        end

        def update_proposal_author
          proposal.coauthorships.clear
          proposal.add_coauthor(form.author)
          proposal.save!
          proposal
        end

        def first_attachment_weight
          return 1 if proposal.photos.count.zero?

          proposal.photos.count
        end
      end
    end
  end
end
