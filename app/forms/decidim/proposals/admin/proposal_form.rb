# frozen_string_literal: true

module Decidim
  module Proposals
    module Admin
      # A form object to be used when admin users want to create a proposal.
      class ProposalForm < Decidim::Proposals::Admin::ProposalBaseForm
        include Decidim::HasUploadValidations
        include Decidim::AttachmentAttributes

        translatable_attribute :title, String do |field, _locale|
          validates field, length: { in: 0..300 }, if: proc { |resource| resource.send(field).present? }
        end
        translatable_attribute :body, String

        attribute :associated_state, String

        validates :title, :body, translatable_presence: false

        attachments_attribute :photos
        attachments_attribute :documents

        validate :notify_missing_attachment_if_errored

        attribute :is_interactive
        attribute :is_hidden
        attribute :image_attachment

        def map_model(model)
          super(model)
          presenter = ProposalPresenter.new(model)

          self.title = presenter.title(all_locales: title.is_a?(Hash))
          self.body = presenter.body(all_locales: body.is_a?(Hash))
          self.is_interactive = model.is_interactive
          self.is_hidden = model.is_hidden
          self.attachment = if model.documents.first.present?
                              { file: model.documents.first.file, title: translated_attribute(model.documents.first.title) }
                            else
                              {}
                            end
          self.image_attachment = model.image_attachment
        end

        def proposal_state_select
          Decidim::Proposals::Proposal.associated_states.keys.map do |associated_state|
            [
              I18n.t("associated_states.#{associated_state}", scope: "decidim.meetings"),
              associated_state
            ]
          end
        end

        private

        def notify_missing_attachment_if_errored
          if errors.any?
            errors.add(:add_photos, :needs_to_be_reattached) if add_photos.present?
            errors.add(:add_documents, :needs_to_be_reattached) if add_documents.present?
          end
        end
      end
    end
  end
end
