namespace :decidim do
  namespace :govbr do
    desc 'Grant id document authorization to all govbr users that do not have it yet'

    task add_authorization_to_govbr_users: :environment do
      Decidim::Govbr::GrantAuthorizationToGovbrUsers.call
    end
  end
end
