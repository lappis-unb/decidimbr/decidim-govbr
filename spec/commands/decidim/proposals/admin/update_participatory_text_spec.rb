# frozen_string_literal: true

require "rails_helper"

module Decidim
  module Proposals
    module Admin
      describe UpdateParticipatoryText do
        include TranslateHelper
        describe "call" do
          let(:organization) { create :organization }
          let(:participatory_space) { create :participatory_process, organization: organization }
          let(:current_component) do
            create(
              :proposal_component,
              participatory_space: participatory_space
            )
          end

          let(:proposals) do
            proposals = create_list(:proposal, 5, component: current_component)
            proposals.each_with_index do |proposal, idx|
              level = Decidim::Proposals::ParticipatoryTextSection::LEVELS.keys[idx]
              proposal.update(participatory_text_level: level)
              proposal.versions.destroy_all
            end
            proposals
          end

          let(:excel_file_id) do
            workbook = RubyXL::Workbook.new
            worksheet = workbook[0]

            # Creating a header row
            headers = %w(Name Matrix Course)
            worksheet.add_cell(0, 0, headers[0])
            worksheet.add_cell(0, 1, headers[1])
            worksheet.add_cell(0, 2, headers[2])

            rows = [
              ['John Doe', '12345', 'Computer Science'],
              ['Jane Smith', '67890', 'Mathematics'],
              ['Alice Johnson', '54321', 'Physics']
            ]

            rows.each_with_index do |row, index|
              row.each_with_index do |cell, cell_index|
                worksheet.add_cell(index + 1, cell_index, cell)
              end
            end

            # Saving the workbook to a file
            temp_file_path = Rails.root.join('tmp', 'test_table.xlsx')
            workbook.write(temp_file_path)

            # Create a signed blob for the file
            signed_blob_id = upload_test_file(temp_file_path, filename: 'test_table.xlsx', content_type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
            # Remove the temporary file
            File.delete(temp_file_path)
            signed_blob_id
          end

          let(:expected_str) do
            "Name\tMatrix\tCourse\nJohn Doe\t12345\tComputer Science\nJane Smith\t67890\tMathematics\nAlice Johnson\t54321\tPhysics"
          end

          let(:image_blob) do
            ActiveStorage::Blob.create_and_upload!(
              io: File.open(Decidim::Dev.test_file("city.jpeg", "image/jpeg"), "rb"),
              filename: "city.jpeg",
              content_type: "image/jpeg"
            )
          end

          let(:image_attachment) do
            {
              "0" => { file: image_blob.signed_id }
            }
          end

          let(:proposal_modifications) do
            modifs = []
            new_positions = [3, 1, 2, 4, 5]
            proposals.each do |proposal|
              modifs << if proposal.table?
                          Decidim::Proposals::Admin::ParticipatoryTextProposalForm.new(
                            id: proposal.id,
                            position: new_positions.shift,
                            title: ::Faker::Books::Lovecraft.fhtagn,
                            body: { en: "" },
                            attachment_file: excel_file_id,
                            is_interactive: false
                          ).with_context(
                            current_participatory_space: current_component.participatory_space,
                            current_component: current_component
                          )
                        elsif proposal.image? || proposal.article?
                          Decidim::Proposals::Admin::ParticipatoryTextProposalForm.new(
                            id: proposal.id,
                            position: new_positions.shift,
                            title: ::Faker::Books::Lovecraft.fhtagn,
                            body: { en: "" },
                            image_attachment: image_attachment,
                            is_interactive: false
                          ).with_context(
                            current_participatory_space: current_component.participatory_space,
                            current_component: current_component
                          )
                        else
                          Decidim::Proposals::Admin::ParticipatoryTextProposalForm.new(
                            id: proposal.id,
                            position: new_positions.shift,
                            title: ::Faker::Books::Lovecraft.fhtagn,
                            body: { en: ::Faker::Books::Lovecraft.fhtagn(number: 5) },
                            is_interactive: false
                          ).with_context(
                            current_participatory_space: current_component.participatory_space,
                            current_component: current_component
                          )
                        end
            end
            modifs
          end
          let(:form) do
            instance_double(
              PreviewParticipatoryTextForm,
              current_component: current_component,
              proposals: proposal_modifications,
              invalid?: false,
              should_create_new_proposal?: false
            )
          end
          let(:command) { described_class.new(form) }

          it "does not create a version for each proposal", versioning: true do
            expect { command.call }.to broadcast(:ok)

            proposals.each do |proposal|
              expect(proposal.reload.versions.count).to be_zero
            end
          end

          describe "when form modifies proposals" do
            context "with valid values" do
              it "persists modifications" do
                expect { command.call }.to broadcast(:ok)
                last_attachment = Decidim::Attachment.last

                proposals.zip(proposal_modifications).each do |proposal, proposal_form|
                  proposal.reload
                  expect(proposal_form.title).to eq translated(proposal.title)
                  # expect(translated(proposal_form.body)).to eq translated(proposal.body) if proposal.article?
                  # expect(translated(proposal.body)).to eq expected_str if proposal.table?
                  expect(proposal.image_attachment).to eq(last_attachment) if proposal.image?
                  expect(proposal_form.position).to eq proposal.position
                  expect(proposal_form.is_interactive).to eq proposal.is_interactive
                end
              end
            end

            context "with invalid values" do
              before do
                proposal_modifications.each { |proposal_form| proposal_form.title = "" }
              end

              it "does not persist modifications and broadcasts invalid" do
                failures = {}
                proposals.each do |proposal|
                  failures[proposal.id] = ["Proposal Title can't be blank"]
                end
                expect { command.call }.to broadcast(:invalid, failures)
              end
            end
          end

          context "when preview form is invalid" do
            let(:form) do
              instance_double(
                PreviewParticipatoryTextForm,
                current_component: current_component,
                proposals: [],
                invalid?: true
              )
            end

            it "broadcasts invalid" do
              expect { command.call }.to broadcast(:invalid)
            end
          end

          context "when form pushes a new proposal" do
            let(:current_user) { create :user, :confirmed, :admin, organization: organization }
            let(:form) do
              instance_double(
                PreviewParticipatoryTextForm,
                current_component: current_component,
                proposals: [],
                proposal_to_add: "section",
                invalid?: false,
                should_create_new_proposal?: true,
                current_user: current_user
              )
            end
            let(:last_proposal) { Decidim::Proposals::Proposal.last }

            it "creates a new proposal" do
              expect { command.call }.to change(Decidim::Proposals::Proposal, :count).by(1)
            end

            it "create a new proposal with pre filled title" do
              I18n.with_locale(:'pt-BR') do
                command.call
                expect(last_proposal.title["pt-BR"]).to eq("Seção")
              end
            end
          end

          context "when proposal is marked to be destroyed" do
            let!(:proposal) { proposals.first }
            let(:proposal_modifications) do
              Array.wrap(
                Decidim::Proposals::Admin::ParticipatoryTextProposalForm.new(
                  id: proposal.id,
                  position: proposal.position,
                  title: ::Faker::Books::Lovecraft.fhtagn,
                  body: { en: ::Faker::Books::Lovecraft.fhtagn(number: 5) },
                  is_interactive: false,
                  deleted: true
                ).with_context(
                  current_participatory_space: current_component.participatory_space,
                  current_component: current_component
                )
              )
            end

            it "deletes the proposal" do
              expect { command.call }.to change(Decidim::Proposals::Proposal, :count).by(-1)
              expect { proposal.reload }.to raise_error(ActiveRecord::RecordNotFound)
            end
          end
        end
      end
    end
  end
end
