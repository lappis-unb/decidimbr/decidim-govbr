# frozen_string_literal: true

require "rails_helper"

module Decidim::Govbr
  describe GrantAuthorizationToGovbrUsers do
    subject { described_class.new }
    let(:authorizations) { Decidim::Authorization.all }

    context "when there are no users" do
      it "does not create any authorization" do
        expect { subject.call }.not_to change(authorizations, :count)
      end
    end

    context "when there are no govbr users" do
      let!(:users) { create_list(:user, 3, :confirmed) }

      it "does not create any authorization" do
        expect { subject.call }.not_to change(authorizations, :count)
      end
    end

    context "when there are govbr users and non govbr users" do
      let!(:users) { create_list(:user, 3, :confirmed) }
      let!(:govbr_users) { create_list(:user, 2, :confirmed) }

      before do
        govbr_users.each do |user|
          user.identities.create!({ provider: :govbr, uid: user.id, organization: user.organization })
        end

        # Creates an authorization not granted
        Decidim::Authorization.create!(user: govbr_users.first, name: "id_documents")
      end

      it "create authorization for all govbr users" do
        expect { subject.call }.to change(authorizations, :count).from(1).to(2)
        expect(authorizations.all?(&:granted?)).to be(true)
      end
    end
  end
end