require "rails_helper"

module Decidim
  module Proposals
    describe ParticipatoryTextProposalCell, type: :cell do
      include TranslateHelper
      include Decidim::SanitizeHelper

      let(:organization) { create :organization }
      let(:participatory_space) { create :participatory_process, organization: organization }
      let(:current_component) do
        create(
          :proposal_component,
          participatory_space: participatory_space
        )
      end
      let(:table_str) do
        {
          en: "Name\tMatrix\tCourse\r\nJohn\t10\tSociology\r\nJane\t8\tMathematics\r\n"
        }
      end
      let(:proposals) do
        proposals = create_list(:proposal, 4, component: current_component)
        proposals.each_with_index do |proposal, idx|
          level = Decidim::Proposals::ParticipatoryTextSection::LEVELS.keys[idx]
          proposal.update(participatory_text_level: level)
          proposal.body = (table_str) if proposal.table?
          proposal.versions.destroy_all
        end
        proposals
      end
      let!(:current_user) { create :user, :confirmed, organization: participatory_space.organization }

      describe "#body" do
        context "when the proposal is a table" do
          let(:proposal) { proposals[3] }
          let(:participatory_text_proposal_cell) { cell("decidim/proposals/participatory_text_proposal", proposal) }

          it "renders the correct HTML for the table" do
            # Access the private method to render the body
            rendered_body = participatory_text_proposal_cell.send(:body)

            expected_html = <<-HTML.strip_heredoc
            <table>
              <tr>
                <td>Name</td>
                <td>Matrix</td>
                <td>Course</td>
              </tr>
              <tr>
                <td>John</td>
                <td>10</td>
                <td>Sociology</td>
              </tr>
              <tr>
                <td>Jane</td>
                <td>8</td>
                <td>Mathematics</td>
              </tr>
            </table>
            HTML

            expect(rendered_body.strip).to eq expected_html.strip
          end
        end

        context "when the proposal is an section or subsection" do
          let(:proposal) { proposals[1] }
          let(:participatory_text_proposal_cell) { cell("decidim/proposals/participatory_text_proposal", proposal) }

          it "dont renders the body" do
            # Access the private method to render the body
            rendered_body = participatory_text_proposal_cell.send(:body)
            expect(rendered_body).to be_nil
          end
        end
      end
    end
  end
end
