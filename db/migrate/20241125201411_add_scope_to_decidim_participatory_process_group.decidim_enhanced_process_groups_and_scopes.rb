# This migration comes from decidim_enhanced_process_groups_and_scopes (originally 20241125201029)
class AddScopeToDecidimParticipatoryProcessGroup < ActiveRecord::Migration[6.1]
  def change
    add_reference :decidim_participatory_process_groups, :decidim_scope, foreign_key: true
  end
end
