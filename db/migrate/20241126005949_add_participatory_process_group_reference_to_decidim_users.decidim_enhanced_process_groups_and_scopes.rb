# This migration comes from decidim_enhanced_process_groups_and_scopes (originally 20241126005522)
class AddParticipatoryProcessGroupReferenceToDecidimUsers < ActiveRecord::Migration[6.1]
  def up
    add_reference :decidim_users, :decidim_participatory_process_group, index: true unless column_exists?(:decidim_users, :decidim_participatory_process_group_id)
    add_column :decidim_users, :decidim_participatory_process_group_role, :string unless column_exists?(:decidim_users, :decidim_participatory_process_group_role)
  end

  def down
    remove_reference :decidim_users, :decidim_participatory_process_group, index: true
    remove_column :decidim_users, :decidim_participatory_process_group_role
  end
end
