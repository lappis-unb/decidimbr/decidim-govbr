class AddSensitiveContentToDecidimCommentsComments < ActiveRecord::Migration[6.1]
  def change
    add_column :decidim_comments_comments, :sensitive_content, :boolean, default: false
  end
end
