class AddIsHiddenToDecidimProposalsProposals < ActiveRecord::Migration[6.1]
  def change
    add_column :decidim_proposals_proposals, :is_hidden, :boolean, default: false
  end
end
