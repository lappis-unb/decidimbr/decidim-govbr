class AddAssociatedStateToDecidimProposalsProposals < ActiveRecord::Migration[6.1]
  def change
    add_column :decidim_proposals_proposals, :associated_state, :integer
  end
end
