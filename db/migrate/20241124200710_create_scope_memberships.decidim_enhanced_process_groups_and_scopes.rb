# This migration comes from decidim_enhanced_process_groups_and_scopes (originally 20241124193619)
class CreateScopeMemberships < ActiveRecord::Migration[6.1]
  def change
    create_table :decidim_epng_scope_memberships do |t|
      t.references :decidim_user, null: false, foreign_key: true, index: true
      t.references :decidim_scope, null: false, foreign_key: true, index: true
      t.string :role, null: false

      t.timestamps
    end

    add_index :decidim_epng_scope_memberships, [:decidim_user_id, :decidim_scope_id], unique: true, name: :decidim_epng_user_and_scopes_uniquiness
  end
end
