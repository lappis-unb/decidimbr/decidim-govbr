# This migration comes from decidim_enhanced_process_groups_and_scopes (originally 20241125144732)
class AddPropertiesToDecidimScopeTypes < ActiveRecord::Migration[6.1]
  def change
    add_column :decidim_scope_types, :has_staff_team, :boolean, default: false, null: false
    add_column :decidim_scope_types, :icon, :string
  end
end
