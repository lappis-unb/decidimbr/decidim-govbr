# frozen_string_literal: true

Decidim::Attributes::TimeWithZone.class_eval do
  private

  def cast_value(value)
    return value unless value.is_a?(String)

    Time.zone.strptime(value, I18n.t("time.formats.decidim_short"))
  rescue ArgumentError
    fallback = super
    return fallback unless fallback.is_a?(Time)

    # This is a fix in decidim-core/lib/decidim/attributes/time_with_zone.rb
    # When a survey is saved in database with ends_at or starts_at with a time zone
    # different from UTC, the parser wrongly casts its value to the current tz
    # but with UTC time
    #
    # See https://api.rubyonrails.org/classes/ActiveSupport/TimeWithZone.html#method-c-new
    #     according to the documentation it expects a time in UTC tz, but it's not happening
    #     in the older version of this snippet:
    #
    #     # decidim-core/lib/decidim/attributes/time_with_zone.rb:22
    #     ActiveSupport::TimeWithZone.new(fallback, Time.zone)
    #
    ActiveSupport::TimeWithZone.new(Time.at(fallback, in: 'UTC'), Time.zone)
  end
end
