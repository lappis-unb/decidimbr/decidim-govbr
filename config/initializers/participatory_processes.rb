Decidim::ParticipatoryProcesses::Engine.class_eval do
  routes do
    get '/process_groups', to: 'participatory_processes#index_with_scopes', as: :process_groups
    get '/process_groups/:process_group_id/process_groups', to: 'participatory_processes#index_with_scopes', as: :process_groups_with_scopes
  end
end
