Decidim.component_registry.find(:meetings).settings(:global) do |settings|
  settings.attribute :attachments_allowed, type: :boolean, default: false
  settings.attribute :enable_comments_attachment, type: :boolean, default: true
end

Decidim::Meetings::Engine.class_eval do
  routes do
    resources :meetings do
      resources :agenda, except: [:index, :destroy]
    end
  end
end

Decidim::Meetings::MeetingSerializer.class_eval do
  def serialize
    {
      id: meeting.id,
      category: {
        id: meeting.category.try(:id),
        name: meeting.category.try(:name)
      },
      scope: {
        id: meeting.scope.try(:id),
        name: meeting.scope.try(:name)
      },
      participatory_space: {
        id: meeting.participatory_space.id,
        url: Decidim::ResourceLocatorPresenter.new(meeting.participatory_space).url
      },
      component: { id: component.id },
      title: meeting.title,
      description: meeting.description,
      start_time: meeting.start_time.to_s,
      end_time: meeting.end_time.to_s,
      attendees: meeting.attendees_count.to_i,
      contributions: meeting.contributions_count.to_i,
      organizations: meeting.attending_organizations,
      address: meeting.address,
      location: meeting.location,
      reference: meeting.reference,
      comments: meeting.comments_count,
      attachments: meeting.attachments.size,
      followers: meeting.follows.size,
      url: url,
      related_proposals: related_proposals,
      related_proposals_ids: related_proposals_ids,
      related_proposals_names: related_proposals_names,
      related_results: related_results,
      related_results_ids: related_results_ids,
      related_results_names: related_results_names,
      published: meeting.published_at.present?
    }
  end

  def related_proposals_names
    meeting.linked_resources(:proposals, "proposals_from_meeting").map do |proposal|
      translated_attribute(proposal.title)
    end
  end

  def related_proposals_ids
    meeting.linked_resources(:proposals, "proposals_from_meeting").map(&:id)
  end

  def related_results_names
    meeting.linked_resources(:results, "meetings_through_proposals").map do |result|
      translated_attribute(result.try(:title))
    end
  end

  def related_results_ids
    meeting.linked_resources(:results, "meetings_through_proposals").map(&:id)
  end
end
